package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    List<FridgeItem> fridgeItems = new ArrayList<>();

    int max_size = 20;
    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            fridgeItems.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeItems.contains(item)) {
            fridgeItems.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        if (nItemsInFridge() > 0) {
            fridgeItems.clear();
        }
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();

        for (FridgeItem item : this.fridgeItems) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        for (FridgeItem item : expiredItems) {
            if (item.hasExpired()) {
                fridgeItems.remove(item);
            }
        }
        return expiredItems;
    }
}

